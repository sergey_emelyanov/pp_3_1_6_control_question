package ru.emelyanov.PP_3_1_6_Control_question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pp316ControlQuestionApplication {
    private static CodeGetter codeGetter;

    @Autowired
    public Pp316ControlQuestionApplication(CodeGetter codeGetter) {
        Pp316ControlQuestionApplication.codeGetter = codeGetter;
    }

    public static void main(String[] args) {
        SpringApplication.run(Pp316ControlQuestionApplication.class, args);
        System.out.println(codeGetter.getCode());
        System.out.println("ssh");
    }
}

