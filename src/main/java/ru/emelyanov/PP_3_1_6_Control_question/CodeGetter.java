package ru.emelyanov.PP_3_1_6_Control_question;

import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.emelyanov.PP_3_1_6_Control_question.model.User;

import java.util.List;

@Component
public class CodeGetter {
    private final String url = "http://94.198.50.185:7081/api/users";
    private final RestTemplate restTemplate = new RestTemplate();
    private HttpHeaders headers;

    public CodeGetter() {
    }

    public String getCode() {
        setSessionIdToHeaders();
        User user = new User(3L, "James", "Brown", (byte) 45);
        return save(user) + update(user) + delete();
    }

    private void setSessionIdToHeaders() {
        ResponseEntity<User[]> responseEntity = restTemplate.getForEntity(url, User[].class);
        HttpHeaders responseHeaders = responseEntity.getHeaders();
        System.out.println("headers -" + responseHeaders);
        List<String> cookies = responseHeaders.get(HttpHeaders.SET_COOKIE);
        String sessionId = cookies.get(0).substring(0, "JSESSIONID=".length() + 32);
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        headers.add(HttpHeaders.COOKIE, sessionId);
    }

    private String save(User user) {
        HttpEntity<User> httpEntity = new HttpEntity<>(user, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        return responseEntity.getBody();
    }

    private String update(User user) {
        user.setName("Thomas");
        user.setLastName("Shelby");
        HttpEntity<User> requestEntity = new HttpEntity<>(user, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);
        return responseEntity.getBody();
    }

    private String delete() {
        HttpEntity<User> request = new HttpEntity<>(new User(), headers);
        ResponseEntity<String> response = restTemplate.exchange(url + "/" + 3, HttpMethod.DELETE, request, String.class);
        return response.getBody();
    }
}
